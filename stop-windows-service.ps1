$service = Get-Service -Name winRM

filter timestamp {"$(Get-Date -Format G): $_"}

if ($service.Status -eq "Stopped"){
    Write-Output "$($service.DisplayName) is already stopped. Exiting..." | timestamp
    Exit 1
}else{
    try{
        Write-Output "Attempting to stop the service $($service.DisplayName)" | timestamp
        Stop-Service -Name $service.Name -ErrorVariable err | Out-Null
        Write-Output "Stopped service $($service.Name)" | timestamp
    }catch{
        Write-Output "Failed to stop the service $($service.DisplayName)" | timestamp
        Write-Output $err
        Exit 1
    }
}