#! /usr/bin/pwsh

<#
Task command: pwsh -file 'disable-non-dc-domains.ps1' -MorphURL "Morpheus Appliance URL" -token "<%=cypher.read('secret/YOUR_TOKEN')%>"
#>

 ###############################
#---Creating script paramters---#
 ############################### 
 
 param (
    [Parameter(Mandatory=$true)]
    [string]$MorphURL,
    [Parameter(Mandatory=$true)]
    [string]$token
)

 #####################################
#---Define functions for the script---# 
 #####################################

 function Write-Log {
    [CmdletBinding()]
    param(
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [string]$Message,

        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [ValidateSet('INFO','WARNING','ERROR')]
        [string]$Severity = 'Information'
    )

    $content = "$Severity"+": "+"$Message" | timestamp
    Write-Output $content #>> $Log_File
}

function Stop-Script {
    [CmdletBinding()]
    param(
        [Parameter()]
        [ValidateNotNullOrEmpty()]
        [ValidateSet('Success','Failure')]
        [string]$Ending = 'Success'
    )
    switch ($Ending) {
        Failure {
            Write-Log $err -Severity Error
            switch ($global:DefaultVIServer){
                $null {
                    continue
                }
                default {
                    Write-Log "Disconnecting from $vCenter"
                    Disconnect-Viserver * -Force -Confirm:$false
                }
            }
            Write-Log "vCenters Inventoried: $Successful_vCenters"
            Write-Log "vCenters Uninventoried: $Failed_vCenters"
            Write-Log "|--------SCRIPT RUN FAILURE--------|" -Severity Error
            Write-Log "|----------END SCRIPT RUN----------|" -Severity Error
            Exit 1
          }
        Default {
            Write-Log "vCenters Inventoried: $Successful_vCenters"
            Write-Log "vCenters Uninventoried: $Failed_vCenters"
            Write-Log "|--------SCRIPT RUN SUCCESS--------|"
            Write-Log "|----------END SCRIPT RUN----------|"
            Exit 0
        }
    }
}

 #####################################
#---Setting script level variables.---#
 #####################################
 
 filter timestamp {"$(Get-Date -Format G): $_"}                                                  #<---Creates a timestamp for any event logged---#

#$Log_File = {Enter log file location}
$Header = @{"Authorization" = "BEARER $token"}
$clouds = Invoke-WebRequest -Method GET -Uri ($MorphURL + '/api/networks/domains?max=10000') -Headers $Header -SkipCertificateCheck | ConvertFrom-Json | Select-Object -ExpandProperty networkDomains
$contentType = "application/json"
$nonDC = $clouds | Where-Object {$_.DomainController -ne $true -and $_.id -ne 1}

foreach ($cloud in $nonDC){
    $id = $cloud.id
    $name = $cloud.name
    $active = $cloud.active
    Write-log "Domain $name with ID $id active state is: $active"
    $content = Invoke-WebRequest -Method GET -Uri ($MorphURL + "/api/networks/domains/$id") -Headers $Header -SkipCertificateCheck | ConvertFrom-Json | Select-Object -ExpandProperty networkDomain
    $data = @{
        networkDomain = @{
            active = "false"
        }
    }
    try {
        Write-Log "Attempting to disable domain $name"
        Invoke-WebRequest -Method PUT -Uri  ($MorphURL + "/api/networks/domains/$id") -ContentType $contentType -Headers $Header -Body ($data|ConvertTo-Json -Depth 10) -SkipCertificateCheck -ErrorAction Stop -ErrorVariable err
    }catch{
        Write-Log "Failed to disable domain $name with the following error:"
        Write-Log $err
        Exit 1
    }
}




