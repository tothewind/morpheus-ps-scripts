param (
    # Hostname for the system
    [Parameter()]
    [string]
    $Hostname,
    # DNS zone to create the record in
    [Parameter()]
    [string]
    $Zone_Name,
    # IP Address for the DNS record
    [Parameter()]
    [string]
    $IP_Address
)

filter timestamp {"$(Get-Date -Format G): $_"} 

try {
    Add-DnsServerResourceRecordA -Name $Hostname -ZoneName $Zone_Name -IPv4Address $IP_Address -CreatePtr -ErrorAction Stop -ErrorVariable err
    exit 0
}catch{
    Write-Output "Failed to create DNS record." | timestamp
    Write-Output $err
    Exit 1
}