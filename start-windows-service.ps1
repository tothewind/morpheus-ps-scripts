$service = Get-Service -Name WinRM

filter timestamp {"$(Get-Date -Format G): $_"}

if ($service.Status -eq "Running"){
    Write-Output "$($service.DisplayName) is already running. Exiting..." | timestamp
    Exit 1
}else{
    try{
        Write-Output "Attempting to start the service $($service.DisplayName)" | timestamp
        Start-Service -Name $service.Name -ErrorVariable err | Out-Null
        Write-Output "Started service $($service.Name)" | timestamp
    }catch{
        Write-Output "Failed to start the service $($service.DisplayName)" | timestamp
        Write-Output $err
        Exit 1
    }
}